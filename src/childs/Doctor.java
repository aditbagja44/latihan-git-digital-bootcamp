/*
 * Created By : Adit Bagja S
 * Created At : 10 August 2023
 */

package childs;
import parents.Person;

// merupakan child class dari class Person
public class Doctor extends Person {
	public String specialist;
	
	public Doctor() {
		// TODO Auto-generated constructor stub
	}
	
	// Super Constructor
	public Doctor(String name, String address, String specialist) {
		super(name, address);
		this.specialist = specialist;
	}

	public void surgery() {
		System.out.println("I can surgery operation Patients");
	}
	
	// Method overriding
	public void greeting() {
		super.greeting(); // memanggil method greeting dari Parent class (Person)
		System.out.println("My occupation is a " + specialist + " doctor");
	}
}
