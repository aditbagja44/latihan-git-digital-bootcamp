package childs;
import parents.Person;

// merupakan child class dari class Person
public class Teacher extends Person {
	public String subject;
	
	public Teacher() {
		// TODO Auto-generated constructor stub
	}
	
	public Teacher(String name, String address, String subject) {
		super(name, address);
		this.subject = subject;
		// TODO Auto-generated constructor stub
	}

	public void teaching() {
		System.out.println("I can teach " + subject + ", So anyone who wants to learn can talk to me.");
	}
	
	// Method overriding
	public void greeting() {
		super.greeting(); // memanggil method greeting dari Parent class (Person)
		System.out.println("My job is a " + subject + " teacher");
	}
}
