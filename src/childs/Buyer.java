package childs;
import parents.Person;

public class Buyer extends Person {
	public String barang;

	public Buyer(String name, String address, String barang) {
		super(name, address);
		this.barang = barang;
	}

	public Buyer() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public void greeting() {
		super.greeting(); // memanggil method greeting dari Parent class (Person)
		System.out.println("Im buying a " + barang + " in the store");
	}
}
