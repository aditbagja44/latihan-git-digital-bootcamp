/*
 * Created By : Adit Bagja S
 * Created At : 10 August 2023
 */
import childs.Doctor;
import childs.Programmer;
import childs.Teacher;
import parents.Person;

public class AppMain {
	public static void main(String[] args) {
		Person person2 = new Programmer("Rizky", "Bandung", ".Net Core");
		Person person3 = new Teacher("Joko","Tegal","Matematika");
		Person person4 = new Doctor("Eko","Surabaya","Dentist");
		
		sayHello(person2);
		sayHello(person3);
		sayHello(person4);
	}
	
	static void sayHello(Person person) {
		String message;
		if(person instanceof Programmer) {
			Programmer programmer = (Programmer) person;
			message = "Hello, " + programmer.name + ". Seorang Programmer " + programmer.technology + ".";
		}else if(person instanceof Teacher ) {
			Teacher teacher = (Teacher) person;
			message = "Hello, " + teacher.name + ". Seorang Guru " + teacher.subject + ".";
		}else if(person instanceof Doctor) {
			Doctor doctor = (Doctor) person;
			message = "Hello, " + doctor.name + ". Seorang Dokter " + doctor.specialist + ".";
		}else {
			message = "Hello, " + person.name + ".";
		}
		System.out.println(message);
	}
}
