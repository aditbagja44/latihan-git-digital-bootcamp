package parents;

public class Person {
	public String name;
	public String address;
	
	// Constructor berparameter yang dibuat IDE Eclipse
	public Person(String name, String address) {
		super();
		this.name = name;
		this.address = address;
	}

	// Constructor default yang dibuat IDE Eclipse
	public Person() {
		super();
	}
	
	// Method void (tidak mengembalikan value)
	public void greeting() {
		System.out.println("Hello my name is " + name + ".");
		System.out.println("I come from " + address + ".");
	}
	
	public void getInformation(String name, String address) {
		this.name = name;
		this.address = address;
	}
}
